const express = require('express');

const app = express();
const port = 3000;

app.get('/', (req, res) => {
  res.send('Hello World 123!');
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});

app.listen(process.env.PORT || 8082, () => {
  console.log('Server is running');
});

module.exports = app;
