const request = require('supertest');
const { describe, it } = require('mocha');
const app = require('../server');

describe('GET /', () => {
  it('respond with Hello World', (done) => {
    request(app).get('/').expect('Hello World 123!', done);
  });
});
